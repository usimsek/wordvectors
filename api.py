import flask
from flask import jsonify, request
import vectorOperations

app = flask.Flask(__name__)

vectorOperations.init()


@app.route('/similarWords', methods=['POST'])
def similarWords():
    jsonData = request.get_json()
    models = jsonData["models"]
    words = jsonData["words"]
    pos = jsonData["pos"]
    return jsonify(vectorOperations.similar_words(models, words, pos))


@app.route('/word2word', methods=['POST'])
def word2word():
    jsonData = request.get_json()
    gold = jsonData["gold"]
    target = jsonData["target"]
    words = jsonData["words"]
    pos = jsonData["pos"]
    return jsonify(vectorOperations.word2word(gold, target, words, pos))


app.run()
