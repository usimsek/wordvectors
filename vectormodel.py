import gensim
import os
import io
from gensim.models.keyedvectors import KeyedVectors
from gensim.models.word2vec import LineSentence
from gensim.models.fasttext import FastText as FT_gensim


def trainModel(modelName, vecSize, trainingPath=None, format=None):

    if trainingPath is None:
        data_dir = '{}'.format(os.sep).join(
            [gensim.__path__[0], 'test', 'test_data']) + os.sep
        train_file = data_dir + 'lee_background.cor'
        vectorSize = 100
    else:
        train_file = trainingPath
        vectorSize = vecSize

    data = LineSentence(train_file)
    model_gensim = FT_gensim(size=vectorSize, bucket=100000)

    model_gensim.build_vocab(data)
    model_gensim.alpha = 0.5
    model_gensim.epochs = 20

    model_gensim.train(
        data, total_examples=model_gensim.corpus_count, epochs=model_gensim.iter)

    model_gensim.wv.save_word2vec_format(
        modelName+'_w2v') if format == "w2v" else model_gensim.wv.save(
            modelName)


def loadModel(modelName):
    model = FT_gensim.load(modelName)
    print(model)
    return model


def convertToBinary(fname):
    model = KeyedVectors.load_word2vec_format(fname, binary=False)
    model.save_word2vec_format(fname+".bin", binary=True)


def vector2model(fname, type, binaryFlag=False):
    if (type == "w2v"):
        return KeyedVectors.load_word2vec_format(
            fname, fvocab=None, binary=binaryFlag)
    else:
        return FT_gensim.load_fasttext_format(fname)


def load_vectors(fname):
    fin = io.open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
    n, d = map(int, fin.readline().split())
    data = {}
    for line in fin:
        tokens = line.rstrip().split(' ')
        data[tokens[0]] = map(float, tokens[1:])
    return data
