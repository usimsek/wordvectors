import vectormodel
import logging
from wordnetOperations import wn_get_pos, lemmatize
import lstm

# for logging
logging.basicConfig(
    format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# vectormodel.trainModel(modelName="hotel_model", vecSize=100,
#                       trainingPath="training.txt", format="w2v")

# vectormodel.convertToBinary(fname="vectors/numberbatch.vec")

models = {}


def init():
    model_conceptnet = vectormodel.vector2model(
        fname="vectors/numberbatch.vec.bin", type="w2v", binaryFlag=True)
#    model_google = vectormodel.vector2model(
#        fname="vectors/GoogleNews-vectors-negative300.bin", type="w2v", binaryFlag=True)
    model_hotel = vectormodel.loadModel(modelName="hotel_model")

    global models
    # models = {"mcn": model_conceptnet, "mg": model_google, "mh": model_hotel}
    models = {"mcn": model_conceptnet, "mh": model_hotel}


def similar_words(_models, words, pos):

    similarWords = set()
    for modelName in _models:
        mostSimilar = models[modelName].wv.most_similar(
            positive=words, topn=len(models[modelName].wv.vocab))
        for word in mostSimilar:
            if word in models[modelName].wv.vocab:
                continue
            if word[1] > 0.5:
                isCorrectPOS = wn_get_pos(word[0], pos)
                if isCorrectPOS:
                    similarWords.add(lemmatize(word[0], pos))
    return list(similarWords)


def word2word(_gold, _target, _words, _pos):

    similar_target = similar_words([_target], _words, _pos)
    similar_gold = similar_words([_gold], _words, _pos)
    similarWords = set(similar_gold)

    for word in similar_gold:
        for word_target in similar_target:
            if word_target in models[_gold].wv.vocab:
                score = models[_gold].wv.similarity(word, word_target)
                if score > 0.5:
                    print(word + "-" + word_target + ": " + str(score))
                    similarWords.add(word_target)

    return list(similarWords)


def sentenceCompletion(words):
    return models["mcn"].predict_output_word(words, topn=5)


def textGenerator(vectorModel, _modelName, _text):
    model = lstm.loadModel(modelName=_modelName)
    print(lstm.generate_sentence(vectorModel, model, _text, 5))


# textGenerator("lstm_embedding", "lstm_model.h5", "I search a hotel")
# lstm.train_word2vec_embedding()
# lstm.train(modelName="lstm_embedding")
# model_hotel_w2v = vectormodel.vector2model(
#    fname="hotel_model_w2v", type="w2v", binaryFlag=True)

# print("ConceptNET:")
# print(model_conceptnet.similarity("search", "find"))
# print("Hotel FT:")
# print(model_hotel.similarity("search", "find"))
# print("Google:")
# print(model_google.similarity("search", "find"))
# print("Hotel w2v: ")
# print(model_hotel_w2v.similarity("search", "find"))

# similarWords = model_hotel.wv.similar_by_word(word="search", topn=10000)

# for word in similarWords:
#    if word[1] > 0.5 and word[0] == "need":
#        print(word)
