# -*- coding: utf-8 -*-

from langdetect import detect
from langdetect import DetectorFactory
from langdetect import lang_detect_exception
DetectorFactory.seed = 0


def getLanguage(text):
    try:
        return detect(u''.join(text))
    except lang_detect_exception.LangDetectException:
        return "none"
