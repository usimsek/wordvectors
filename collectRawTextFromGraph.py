# -*- coding: utf-8 -*-

from SPARQLWrapper import SPARQLWrapper, JSON
from identifyLang import getLanguage
from gensim.summarization.textcleaner import get_sentences
import gensim.parsing.preprocessing as preprocess


sparql = SPARQLWrapper("http://graphdb.sti2.at:8080/repositories/TirolGraph-Alpha")

with open('sparqlquery.txt', 'r') as f:
    query = f.read()

sparql.setQuery(query)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()
print("results are back")
with open("training_new.txt", "a+") as fw:
    for result in results["results"]["bindings"]:
        desc = result["desc"]["value"]
        if getLanguage(desc) == "en":
            clean_desc_list = []
            for sentence in get_sentences(desc):
                cleanSentence = preprocess.strip_non_alphanum(preprocess.strip_punctuation(preprocess.strip_tags(
                    preprocess.strip_multiple_whitespaces(sentence))))
                cleanSentence = cleanSentence+'.\n'
                clean_desc_list.append(cleanSentence)

            print(u''.join(clean_desc_list).encode('utf-8'))
            fw.write(u''.join(clean_desc_list).encode("utf-8"))
