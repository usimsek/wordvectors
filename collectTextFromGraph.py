# -*- coding: utf-8 -*-

from SPARQLWrapper import SPARQLWrapper, JSON
from identifyLang import getLanguage
from gensim.utils import lemmatize
from gensim.summarization.textcleaner import get_sentences
import gensim.parsing.preprocessing as preprocess
import re

sparql = SPARQLWrapper("http://graphdb.sti2.at:8080/repositories/TirolGraph-Alpha")

with open('sparqlquery.txt', 'r') as f:
    query = f.read()

sparql.setQuery(query)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

for result in results["results"]["bindings"]:
    desc = result["desc"]["value"]
    if getLanguage(desc) == "en":
        clean_desc_list = []
        for sentence in get_sentences(desc):
            lemmas = lemmatize(preprocess.strip_tags(
                preprocess.strip_multiple_whitespaces(sentence)))

            cleanSentence = (
                ' '.join([re.sub(
                    pattern=r"/[^/]*$", repl='', string=l) for l in lemmas]))

            clean_desc_list.append(cleanSentence)

        clean_desc = '.\n'.join(clean_desc_list)
        with open("training_new.txt", "a+") as fw:
            fw.write(u''.join(clean_desc).encode("utf-8"))
