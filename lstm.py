# -*- coding: utf-8 -*-
from __future__ import print_function

from keras.layers.recurrent import LSTM
from keras.layers.embeddings import Embedding
from keras.layers import Dense, Activation
from keras.models import Sequential
from keras.callbacks import LambdaCallback
from keras.models import load_model


import numpy as np
import gensim
import string

word_model = gensim.models.Word2Vec()
sentences = []
max_sentence_len = 0
model = Sequential()


def prepareSentences():
    print('\nFetching the text...')
    path = "training_new.txt"

    print('\nPreparing the sentences...')
    global max_sentence_len
    max_sentence_len = 40
    with open(path) as file_:
        docs = file_.readlines()
        global sentences
        sentences = [[word for word in doc.lower().translate(None, string.punctuation).split()[
            :max_sentence_len]] for doc in docs]
        print('Num sentences:', len(sentences))


def train_word2vec_embedding():

    prepareSentences()

    print('\nTraining word2vec...')
    global word_model
    word_model = gensim.models.Word2Vec(sentences, size=100, min_count=1, window=5, iter=100)
    word_model.wv.save_word2vec_format("lstm_embedding", binary=True)


def word2idx(word):
    return word_model.wv.vocab[word].index


def idx2word(idx):
    return word_model.wv.index2word[idx]


def sample(preds, temperature=1.0):
    if temperature <= 0:
        return np.argmax(preds)
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)


def generate_next(text, num_generated=10):
    word_idxs = [word2idx(word) for word in text.lower().split()]
    for i in range(num_generated):
        prediction = model.predict(x=np.array(word_idxs))
        idx = sample(prediction[-1], temperature=0.7)
        word_idxs.append(idx)
    return ' '.join(idx2word(idx) for idx in word_idxs)


def generate_sentence(vectorModel, _model, text, num_generated=10):
    global word_model
    word_model = gensim.models.KeyedVectors.load_word2vec_format(fname=vectorModel, binary=True)
    word_idxs = [word2idx(word) for word in text.lower().split()]
    for i in range(num_generated):
        prediction = _model.predict(x=np.array(word_idxs))
        idx = sample(prediction[-1], temperature=0.7)
        word_idxs.append(idx)
    return ' '.join(idx2word(idx) for idx in word_idxs)


def on_epoch_end(epoch, _):
    print('\nGenerating text after epoch: %d' % epoch)
    texts = [
        'I am looking for a hotel ',
        'find a hotel',
        'searching a hotel with wifi',
        'a hotel ',
    ]
    for text in texts:
        sample = generate_next(text)
        print('%s... -> %s' % (text, sample))


def train(modelName):
    prepareSentences()
    global word_model
    word_model = gensim.models.KeyedVectors.load_word2vec_format(fname=modelName, binary=True)
    pretrained_weights = word_model.wv.syn0
    vocab_size, emdedding_size = pretrained_weights.shape
    print('Result embedding shape:', pretrained_weights.shape)
    print('Checking similar words:')
    for word in ['search', 'hotel', 'wifi', 'innsbruck']:
        most_similar = ', '.join('%s (%.2f)' % (similar, dist)
                                 for similar, dist in word_model.most_similar(word)[:8])
        print('  %s -> %s' % (word.encode("utf-8"), most_similar.encode("utf-8")))

    print('\nPreparing the data for LSTM...')
    train_x = np.zeros([len(sentences), max_sentence_len], dtype=np.int32)
    train_y = np.zeros([len(sentences)], dtype=np.int32)
    for i, sentence in enumerate(sentences):
        if not sentence:
            continue
        for t, word in enumerate(sentence[:-1]):
            train_x[i, t] = word2idx(word)
        train_y[i] = word2idx(sentence[-1])
    print('train_x shape:', train_x.shape)
    print('train_y shape:', train_y.shape)

    print('\nTraining LSTM...')
    global model
    model.add(Embedding(input_dim=vocab_size,
                        output_dim=emdedding_size, weights=[pretrained_weights]))
    model.add(LSTM(units=emdedding_size))
    model.add(Dense(units=vocab_size))
    model.add(Activation('softmax'))
    print("compiling the LSTM model")
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy')

    model.fit(train_x, train_y,
              batch_size=128,
              epochs=20,
              callbacks=[LambdaCallback(on_epoch_end=on_epoch_end)])

    model.save("lstm_model.h5")


def loadModel(modelName):
    return load_model(modelName)
