from nltk.corpus import wordnet
from nltk.stem.wordnet import WordNetLemmatizer


def wn_synonyms(word, _pos):
    syns = wordnet.synsets(word, pos=_pos)
    for syn in syns:
        lemmas = syn.lemmas()
        for lemma in lemmas:
            print(lemma.name())


def wn_get_pos(word, _pos):
    syns = wordnet.synsets(word, pos=_pos)
    print(syns)
    if syns:
        return True


def lemmatize(_word, _pos):
    return WordNetLemmatizer().lemmatize(_word, _pos)
